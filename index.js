const express = require("express")

var session = require("express-session");
const server = express();


// 托管静态页面
server.use(express.static('./pages'))
// 解析 POST 提交过来的表单数据
server.use(express.urlencoded({ extended: false }))


server.use(session({
    secret:"keyboard test demo",   // secret 属性的值可以为任意字符电
    resave: false,           // 固定写法
    saveUninitialized: true, // 因定写法
}))

// 只要配置成功了 express-jwt 这个中间件，就可以把解析出来的用户信息，挂载到 req.user 属性上
const {expressjwt} = require('express-jwt')
const secretKey = "taishangaicai No1 ^_^ ";
server.use(expressjwt({ 
  secret: secretKey, algorithms: ['HS256'] 
  }).unless({ path:[/^\/api\//] }))

const sessionRouter = require("./router/session")
server.use("/api", sessionRouter)




const jwtRouter = require("./router/jwt")
server.use(jwtRouter)
server.use((err, req, res, next) => {
    // 这次错误是由 token 解析失败导致的
    if (err.name === 'UnauthorizedError') {
      return res.send({
        status: 1,
        message: '无效的token',
      })
    }
    res.send({
      status: 2,
      message: '未知的错误',
    })
})

server.listen(8080,()=>{
    console.log("http://127.0.0.1:8080 is running!")
})