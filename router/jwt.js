const express = require("express")
// 实例化路由
const apiRouter =  express.Router()


const jwt = require('jsonwebtoken')
const secretKey = "taishangaicai No1 ^_^ "

// jwt 验证 去除 session 的影响 
apiRouter.use((req, res, next) =>{
    req.session.islogin = true;
    next()
})

apiRouter.post("/api/login", (req, res) => {
    console.log("login", req.body.username)
    // 判断用户提交的登录信息是否正确
    if (!req.body.username && !req.body.password) {
      return res.send({ status: 1, msg:"查询失败"})
    }
    const tokenStr = jwt.sign({ username: req.body.username }, secretKey, { expiresIn: '10h' })
    res.send({
        status: 0,
        message: '登录成功！',
        result:{
            token: tokenStr, // 要发送给客户端的 token 字符串
            username: req.body.username
        }
    })
})

// 获取用户姓名的接口
apiRouter.get( '/admin/username' , (req, res) => {
    res.send({
        status: 0,
        message: '获取用户信息成功！',
        result:{
            username: req.auth.username
        }, // 要发送给客户端的用户信息
    })
})


module.exports = apiRouter