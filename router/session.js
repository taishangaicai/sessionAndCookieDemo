const express = require("express")
// 实例化路由
const apiRouter =  express.Router()


apiRouter.post("/session/login", (req, res) => {
    // 判断用户提交的登录信息是否正确
    if (!req.body.userName && !req.body.password) {
      return res.send({ status: 1, msg:"查询失败"})
    }
  
    req.session.user = req.body // 将用户的信息，存储到 session 中
    req.session.islogin = true  // 将用户的登录状态，存储到 session 中
    res.send({ status: 0, msg:"登录成功"})
})

// 获取用户姓名的接口
apiRouter.get( '/session/username' , (req, res) => {
    // 判断用户是否登录
    if (!req.session.islogin) {
      return res.send({ status: 1, msg: 'fail'})
    }
    res.send({ 
      status: 0,
      msg: 'success', 
      username: req.session.user.username 
    })
})

// 退出登录的接口
apiRouter.post("/session/logout", (req, res) => {
    // 清空当前客户端对应的 session 信息
    req.session.destroy();
    res.send({
        status: 0,
        msg:"退出成功！"
    })
})

module.exports = apiRouter